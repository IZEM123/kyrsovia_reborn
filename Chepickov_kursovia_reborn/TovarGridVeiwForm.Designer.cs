﻿
namespace Chepickov_kursovia_reborn
{
    partial class TovarGridVeiwForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TovarsGridView = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.title = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.provider = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Edit_b = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Delete = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.TovarsGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // TovarsGridView
            // 
            this.TovarsGridView.AllowUserToAddRows = false;
            this.TovarsGridView.AllowUserToDeleteRows = false;
            this.TovarsGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.TovarsGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TovarsGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.title,
            this.provider,
            this.price,
            this.count,
            this.Edit_b,
            this.Delete});
            this.TovarsGridView.Location = new System.Drawing.Point(12, 12);
            this.TovarsGridView.Name = "TovarsGridView";
            this.TovarsGridView.Size = new System.Drawing.Size(776, 426);
            this.TovarsGridView.TabIndex = 0;
            this.TovarsGridView.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.ServicesGridView_CellMouseClick);
            // 
            // id
            // 
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            // 
            // title
            // 
            this.title.HeaderText = "Наименование";
            this.title.Name = "title";
            this.title.ReadOnly = true;
            // 
            // provider
            // 
            this.provider.HeaderText = "Поставщик";
            this.provider.Name = "provider";
            this.provider.ReadOnly = true;
            // 
            // price
            // 
            this.price.HeaderText = "Цена";
            this.price.Name = "price";
            this.price.ReadOnly = true;
            // 
            // count
            // 
            this.count.HeaderText = "Количество";
            this.count.Name = "count";
            this.count.ReadOnly = true;
            // 
            // Edit_b
            // 
            this.Edit_b.HeaderText = "Edit";
            this.Edit_b.Name = "Edit_b";
            // 
            // Delete
            // 
            this.Delete.HeaderText = "Delete";
            this.Delete.Name = "Delete";
            // 
            // TovarGridVeiwForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.TovarsGridView);
            this.Name = "TovarGridVeiwForm";
            this.Text = "TovarGridVeiwForm";
            this.Load += new System.EventHandler(this.TovarGridVeiwForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TovarsGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView TovarsGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn title;
        private System.Windows.Forms.DataGridViewTextBoxColumn provider;
        private System.Windows.Forms.DataGridViewTextBoxColumn price;
        private System.Windows.Forms.DataGridViewTextBoxColumn count;
        private System.Windows.Forms.DataGridViewButtonColumn Edit_b;
        private System.Windows.Forms.DataGridViewButtonColumn Delete;
    }
}