﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kursovaya_chepikov.DataBase
{
    public class Tovar
    {
        public int id;
        public string title;
        public string provider;
        public int price;
        public int count; 

        override
        public string ToString()
        {
            return $"{title} - {price} руб";
        }
    }
}
