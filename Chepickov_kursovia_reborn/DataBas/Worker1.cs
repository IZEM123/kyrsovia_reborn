﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kursovaya_chepikov.DataBase
{
    class Worker1
    {
        public int id;
        public string FullName;
        public string login;
        public string password;
        public role workerRole;
        public enum role
        {
            client,
            admin,
            worker
        }

        static public string getRoleName(role role)
        {
            switch (role)
            {
                case role.client:
                    return "client";
                case role.admin:
                    return "admin";
                case role.worker:
                    return "worker";
                default:
                    return "Client";

            }
        }

        static public role getRoleByName(string roleName)
        {
            switch (roleName)
            {
                case "client":
                    return role.client;
                case "worker":
                    return role.worker;
                case "admin":
                    return role.admin;
                default:
                    return role.client;
            }
        }
    }
}
