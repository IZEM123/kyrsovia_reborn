﻿using Chepickov_kursovia_reborn.DataBas;
using kursovaya_chepikov.DataBase;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kursovaya_chepikov
{
    class DataBaseControl
    {
        
        static List<Worker1> users = new List<Worker1>();
        static List<Tovar> tovars = new List<Tovar>();
        static List<Order> orders = new List<Order>(); 

        private static int lastUserid;
        private static int lastTovarid;
        private static int lastOrderid;

        const string userDBFilePath = @".\workers.json";
        const string tovarsDBFilePath = @".\tovars.json";
        const string ordersDBFilePath = @".\orders.json";
        public void LoadDataBase()
        {
            repairDataBase();

            // чтение из файла
            using (FileStream fstream = File.OpenRead(userDBFilePath))
            {
                // преобразуем строку в байты
                byte[] array = new byte[fstream.Length];
                // считываем данные
                fstream.Read(array, 0, array.Length);
                // декодируем байты в строку
                string textFromFile = System.Text.Encoding.UTF8.GetString(array);

                users = JsonConvert.DeserializeObject<List<Worker1>>(textFromFile);

                int maxIndex = 0;
                for(int i = 0; i<users.Count; i++)
                {
                    if(maxIndex < users[i].id)
                    {
                        maxIndex = users[i].id;
                    }
                }
                lastUserid = maxIndex;

            }

            // чтение из файла
            using (FileStream fstream = File.OpenRead(ordersDBFilePath))
            {
                // преобразуем строку в байты
                byte[] array = new byte[fstream.Length];
                // считываем данные
                fstream.Read(array, 0, array.Length);
                // декодируем байты в строку
                string textFromFile = System.Text.Encoding.UTF8.GetString(array);

                orders = JsonConvert.DeserializeObject<List<Order>>(textFromFile);

                int maxIndex = 0;
                for (int i = 0; i < orders.Count; i++)
                {
                    if (maxIndex < orders[i].id)
                    {
                        maxIndex = orders[i].id;
                    }
                }
                lastOrderid = maxIndex;

            }

            using (FileStream fstream = File.OpenRead(tovarsDBFilePath))
            {
                // преобразуем строку в байты
                byte[] array = new byte[fstream.Length];
                // считываем данные
                fstream.Read(array, 0, array.Length);
                // декодируем байты в строку
                string textFromFile = System.Text.Encoding.UTF8.GetString(array);

                tovars = JsonConvert.DeserializeObject<List<Tovar>>(textFromFile);

                int maxIndex = 0;
                for (int i = 0; i < tovars.Count; i++)
                {
                    if (maxIndex < tovars[i].id)
                    {
                        maxIndex = tovars[i].id;
                    }
                }
                lastTovarid = maxIndex;


            }
        }


        private void repairDataBase()
        {
          
            FileInfo fileInf = new FileInfo(userDBFilePath);
            if (!fileInf.Exists)
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Converters.Add(new JavaScriptDateTimeConverter());
                serializer.NullValueHandling = NullValueHandling.Ignore;

                List<Worker1> users = new List<Worker1>();
                Worker1 mainAdmin = new Worker1();
                mainAdmin.id = 1;
                mainAdmin.FullName = "Chepikov Vadim Olegovich";
                mainAdmin.login = "Izem";
                mainAdmin.password = "qwaszx123";
                mainAdmin.workerRole = Worker1.role.admin;

                users.Add(mainAdmin);

                using (StreamWriter sw = new StreamWriter(userDBFilePath))
                using (JsonWriter writer = new JsonTextWriter(sw))
                {
                    serializer.Serialize(writer, users);
                   
                }
            }

            FileInfo tovarfileInf = new FileInfo(tovarsDBFilePath);
            if (!tovarfileInf.Exists)
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Converters.Add(new JavaScriptDateTimeConverter());
                serializer.NullValueHandling = NullValueHandling.Ignore;

                List<Tovar> tovars = new List<Tovar>();
              

                using (StreamWriter sw = new StreamWriter(tovarsDBFilePath))
                using (JsonWriter writer = new JsonTextWriter(sw))
                {
                    serializer.Serialize(writer, tovars);
                   
                }
            }

            FileInfo orderfileInf = new FileInfo(ordersDBFilePath);
            if (!orderfileInf.Exists)
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Converters.Add(new JavaScriptDateTimeConverter());
                serializer.NullValueHandling = NullValueHandling.Ignore;

                List<Order> orders = new List<Order>();


                using (StreamWriter sw = new StreamWriter(ordersDBFilePath))
                using (JsonWriter writer = new JsonTextWriter(sw))
                {
                    serializer.Serialize(writer, orders);

                }
            }
        }


        public void SaveDataBase()
        {
            JsonSerializer serializer = new JsonSerializer();
            serializer.Converters.Add(new JavaScriptDateTimeConverter());
            serializer.NullValueHandling = NullValueHandling.Ignore;

            using (StreamWriter sw = new StreamWriter(userDBFilePath))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, users);
            }
            using (StreamWriter sw = new StreamWriter(tovarsDBFilePath))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, tovars);
            }
            using (StreamWriter sw = new StreamWriter(ordersDBFilePath))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, orders);
            }
        }

        public void AuthorizateUser(string login, string password)
        {
            ProgramState pState = new ProgramState();
            for(int i=0; i<users.Count; i++)
            {
                if (users[i].login == login && users [i].password == password)
                {
                    pState.setAuthorizedUser(users[i]);
                    return;
                }
            }

            throw new Exception("User_не_найден");
        }
         
        public List<Worker1> getUsers()
        {
            return users;
        }

        public void UpdateUser ( int userId,Worker1 newUser)
        {
            users[userId] = newUser;
            SaveDataBase();
        }

        public void DeleteUser(int userId)
        {
            users.RemoveAt(userId);
            SaveDataBase();
        }


        public void AddUser(Worker1 newUser)
        {
            newUser.id = ++lastUserid;
            users.Add(newUser);
            SaveDataBase();
        }



        public void AddTovar(Tovar newTovar)
        {
            newTovar.id = ++lastTovarid;
            tovars.Add(newTovar);
            SaveDataBase();
        }

        public List<Tovar> GetTovars()
        {
            return tovars;
        }
        public void UpdateTovar(int tovarId, Tovar newtovar)
        {
            tovars[tovarId] = newtovar;
            SaveDataBase();
        }

        public void DeleteTovar(int tovarId)
        {
            tovars.RemoveAt(tovarId);
            SaveDataBase();
        }

        public List<Order> GetOrders()
        {
            return orders;
        }

        public Tovar GetTovarById(int tovarId)
        {
            for(int i = 0; i<tovars.Count; i++)
            {
                if(tovars[i].id == tovarId)
                {
                    return tovars[i];
                }
            }
            return null;
        }

        public void AddOrder(Order newOrder)
        {
            newOrder.id = ++lastOrderid;
            orders.Add(newOrder);
            for(int i =0; i<newOrder.tovars.Count; i++)
            {
                int tovarId = newOrder.tovars[i].tovar.id;
                for (int j = 0; j < tovars.Count; j++)
                {
                    if (tovars[j].id == tovarId)
                    {
                        tovars[j].count -= newOrder.tovars[i].count;
                    }
                }
            }
            SaveDataBase();
        }
    }
}
