﻿using kursovaya_chepikov;
using kursovaya_chepikov.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Chepickov_kursovia_reborn
{
    public partial class TovarGridVeiwForm : TemplateForm
    {
        bool isRowEdit = false;
        int rowEditNumber = -1;
        bool isClient;
        public TovarGridVeiwForm(bool isClient = false)
        {
            InitializeComponent();
            this.isClient = isClient;
        }

        private void TovarGridVeiwForm_Load(object sender, EventArgs e)
        {
            loadAndDisplayServicesFromDB();
        }

        private void loadAndDisplayServicesFromDB()
        {
            DataBaseControl dbController = new DataBaseControl();
            List<Tovar> tovars = dbController.GetTovars();


            displayUsers(tovars);
        }
        private void displayUsers(List<Tovar> services)
        {
            TovarsGridView.Rows.Clear();
            for (int i = 0; i < services.Count; i++)
            {
                TovarsGridView.Rows.Add(services[i].id, services[i].title, services[i].provider, services[i].price, services[i].count, "Редактировать", "Удалить");
            }
        }

        private void ServicesGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if(e.RowIndex < 0 || isClient)
            {
                return;
            }
            ProgramState pState = new ProgramState();
            Worker1 authorizedUser = pState.GetAuthorizedUser();
            if (authorizedUser.workerRole == Worker1.role.admin)
            {


                if (e.ColumnIndex == 5)
                {
                    if (!isRowEdit)
                    {
                        startRowEdit(e.RowIndex);
                    }
                    else
                    {
                        if (rowEditNumber == e.RowIndex)
                        {
                            saveRow();
                        }
                        else
                        {
                            saveRow();
                            startRowEdit(e.RowIndex);
                        }
                    }
                }
                if (e.ColumnIndex == 6)
                {
                    deleteRow(e.RowIndex);
                }
            }
            else
            {
                if (e.ColumnIndex == 5)
                {
                    if (!isRowEdit)
                    {
                        startRowEdit(e.RowIndex,true);
                    }
                    else
                    {
                        if (rowEditNumber == e.RowIndex)
                        {
                            saveRow();
                        }
                        else
                        {
                            saveRow();
                            startRowEdit(e.RowIndex,true);
                        }
                    }
                }
            }
        }

        private void startRowEdit(int row, bool isWorker = false)
        {
            try
            {
                isRowEdit = true;
                rowEditNumber = row;
                if (!isWorker)
                {
                    TovarsGridView.Rows[row].Cells[1].ReadOnly = false;
                    TovarsGridView.Rows[row].Cells[2].ReadOnly = false;
                    TovarsGridView.Rows[row].Cells[3].ReadOnly = false;
                }
                TovarsGridView.Rows[row].Cells[4].ReadOnly = false;

                TovarsGridView.Rows[row].Cells[5].Value = "Сохранить";
            }
            catch(Exception) { }
        }

        private void saveRow()
        {
            
            Tovar service = new Tovar();
            service.title = TovarsGridView.Rows[rowEditNumber].Cells[1].Value.ToString();
            service.provider = TovarsGridView.Rows[rowEditNumber].Cells[2].Value.ToString();
            service.price = Convert.ToInt32(TovarsGridView.Rows[rowEditNumber].Cells[3].Value.ToString());
            service.count = Convert.ToInt32(TovarsGridView.Rows[rowEditNumber].Cells[4].Value.ToString());



            TovarsGridView.Rows[rowEditNumber].Cells[1].ReadOnly = true;
            TovarsGridView.Rows[rowEditNumber].Cells[2].ReadOnly = true;
            TovarsGridView.Rows[rowEditNumber].Cells[3].ReadOnly = true;
            TovarsGridView.Rows[rowEditNumber].Cells[4].ReadOnly = true;

            TovarsGridView.Rows[rowEditNumber].Cells[5].Value = "Редактировать";

            DataBaseControl dbController = new DataBaseControl();

            dbController.UpdateTovar(rowEditNumber, service);

            isRowEdit = false;
            rowEditNumber = -1;

        }

        private void deleteRow(int rowId)
        {
            DataBaseControl dbController = new DataBaseControl();

            dbController.DeleteTovar(rowId);

            loadAndDisplayServicesFromDB();
        }
    }
}
