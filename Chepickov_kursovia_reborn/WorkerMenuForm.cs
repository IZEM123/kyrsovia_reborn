﻿using kursovaya_chepikov;
using kursovaya_chepikov.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Chepickov_kursovia_reborn
{
    public partial class WorkerMenuForm : TemplateForm
    {
        public WorkerMenuForm()
        {
            InitializeComponent();
            ProgramState pState = new ProgramState();
            Worker1 authorizedUser = pState.GetAuthorizedUser();
            if (authorizedUser.workerRole == Worker1.role.worker)
            {
                AddTovar_b.Visible = false;
                GridView_b.Visible = false;
                AddWorker_b.Visible = false;
                
            }

        }

        private void WorkerMenuForm_Load(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            WorkerAddForm form = new WorkerAddForm();
            this.Hide();
            form.Show();
            form.Owner = this;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            UsersGridViewForm form = new UsersGridViewForm();
            this.Hide();
            form.Show();
            form.Owner = this;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            TovarGridVeiwForm form = new TovarGridVeiwForm();
            this.Hide();
            form.Show();
            form.Owner = this;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            TovarAddForm form = new TovarAddForm();
            this.Hide();
            form.Show();
            form.Owner = this;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            OrderForm form = new OrderForm();
            this.Hide();
            form.Show();
            form.Owner = this;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            OrdersGridViewForm form = new OrdersGridViewForm();
            this.Hide();
            form.Show();
            form.Owner = this;
        }
    }
}
