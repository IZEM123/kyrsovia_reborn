﻿using Chepickov_kursovia_reborn.DataBas;
using kursovaya_chepikov;
using kursovaya_chepikov.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Chepickov_kursovia_reborn
{
    public partial class CheckForm : TemplateForm
    {
        public CheckForm(Order order)
        {
            InitializeComponent();
            ProgramState pState = new ProgramState();
            Worker1 user = pState.GetAuthorizedUser();
            string text = 
                $@"                         ООО""Paper""   
                            Кассир:{user.FullName}

";
            for(int i = 0; i < order.tovars.Count; i++)
            {
                text += $"{order.tovars[i].tovar.title} X  {order.tovars[i].count}.........Стоимость: {order.tovars[i].count * order.tovars[i].tovar.price}\n\n";

            }
            text += "=================================\n\n";
            double totalSum = 0;

            for (int i = 0; i < order.tovars.Count; i++)
            {
                totalSum += order.tovars[i].tovar.price * order.tovars[i].count;
            }
            text += $"ИТОГ = {totalSum} ";
            CheckBox.Text = text;

        }

        private void CheckForm_Load(object sender, EventArgs e)
        {

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Отправлено в печать");
        }
    }
}
