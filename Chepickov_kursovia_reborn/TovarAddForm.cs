﻿using kursovaya_chepikov;
using kursovaya_chepikov.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Chepickov_kursovia_reborn
{
    public partial class TovarAddForm : TemplateForm
    {
        public TovarAddForm()
        {
            InitializeComponent();
        }

        private void TovarAddForm_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string title = TitleInput.Text.Trim();
            string provider = ProviderInput.Text.Trim();
            int price = Convert.ToInt32(PriceInput.Value);
            int count = Convert.ToInt32(CountInput.Value);

            if (title != "" && provider != "" && price >= 0 && count > 0)
            {
                Tovar newTovar = new Tovar();
                newTovar.title = title;
                newTovar.provider = provider;
                newTovar.price = price;
                newTovar.count = count;

                DataBaseControl dbController = new DataBaseControl();
                dbController.AddTovar(newTovar);
                this.Close();
            }
        }
    }
}
