﻿using kursovaya_chepikov;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Chepickov_kursovia_reborn
{
    public partial class MainMenuForm : TemplateForm
    {
        public MainMenuForm()
        {
            InitializeComponent();
            DataBaseControl dbController = new DataBaseControl();
            dbController.LoadDataBase();
        }

        private void MainMenuForm_Load(object sender, EventArgs e)
        {

        }

        private void Worker_b_Click(object sender, EventArgs e)
        {
            WorkerAuthorizationForm form = new WorkerAuthorizationForm();
            this.Hide();
            form.Show();
            form.Owner = this;
        }

        private void Client_b_Click(object sender, EventArgs e)
        {
            ClientMenuForm form = new ClientMenuForm();
            this.Hide();
            form.Show();
            form.Owner = this;
        }
    }
}
