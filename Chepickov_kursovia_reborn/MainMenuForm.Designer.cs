﻿
namespace Chepickov_kursovia_reborn
{
    partial class MainMenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Worker_b = new System.Windows.Forms.Button();
            this.Client_b = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Worker_b
            // 
            this.Worker_b.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Worker_b.Location = new System.Drawing.Point(594, 189);
            this.Worker_b.Name = "Worker_b";
            this.Worker_b.Size = new System.Drawing.Size(121, 74);
            this.Worker_b.TabIndex = 1;
            this.Worker_b.Text = "Работник";
            this.Worker_b.UseVisualStyleBackColor = true;
            this.Worker_b.Click += new System.EventHandler(this.Worker_b_Click);
            // 
            // Client_b
            // 
            this.Client_b.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Client_b.Location = new System.Drawing.Point(158, 189);
            this.Client_b.Name = "Client_b";
            this.Client_b.Size = new System.Drawing.Size(121, 74);
            this.Client_b.TabIndex = 2;
            this.Client_b.Text = "Клиент";
            this.Client_b.UseVisualStyleBackColor = true;
            this.Client_b.Click += new System.EventHandler(this.Client_b_Click);
            // 
            // MainMenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Client_b);
            this.Controls.Add(this.Worker_b);
            this.Name = "MainMenuForm";
            this.Text = "Главное меню";
            this.Load += new System.EventHandler(this.MainMenuForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Worker_b;
        private System.Windows.Forms.Button Client_b;
    }
}