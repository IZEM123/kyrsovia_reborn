﻿using kursovaya_chepikov;
using kursovaya_chepikov.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Chepickov_kursovia_reborn
{
    public partial class WorkerAddForm : TemplateForm
    {
        public WorkerAddForm()
        {
            InitializeComponent();
        }

        private void WorkerAddForm_Load(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void Submit_b_Click(object sender, EventArgs e)
        {
            string fullName = FullNameInput.Text.Trim();
            string login = LoginInput.Text.Trim();
            string password = PasswordInput.Text.Trim();
            string userRole = "";
            if (WorkerRoleComboBox.SelectedItem != null)
            {
                userRole = WorkerRoleComboBox.SelectedItem.ToString().Trim();
            }

            if (fullName != "" && login != "" && password != "" && userRole != "")
            {
                Worker1 newUser = new Worker1();
                newUser.FullName = fullName;
                newUser.login = login;
                newUser.password = password;
                newUser.workerRole = Worker1.getRoleByName(userRole);


                DataBaseControl dbController = new DataBaseControl();
                dbController.AddUser(newUser);

                this.Close();
            }
        }
    }
}
