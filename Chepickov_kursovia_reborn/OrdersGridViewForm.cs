﻿using Chepickov_kursovia_reborn.DataBas;
using kursovaya_chepikov;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Chepickov_kursovia_reborn
{
    public partial class OrdersGridViewForm : TemplateForm
    {
        bool isRowEdit = false;
        int rowEditNumber = -1;
        public OrdersGridViewForm()
        {
            InitializeComponent();
        }

        private void OrdersGridViewForm_Load(object sender, EventArgs e)
        {
               loadAndDisplayUsersFromDB();
        }
        private void loadAndDisplayUsersFromDB()
        {
            DataBaseControl dbController = new DataBaseControl();
            List<Order> orders = dbController.GetOrders();

            displayUsers(orders);
        }
        private void displayUsers(List<Order> orders)
        {
            OrdersGridView.Rows.Clear();
            for (int i = 0; i < orders.Count; i++)
            {
                string tovarsList = "";
                for(int j = 0; j < orders[i].tovars.Count; j++)
                {
                    tovarsList += $"{orders[i].tovars[j].tovar.title} - {orders[i].tovars[j].tovar.price * orders[i].tovars[j].count} руб. | ";
                }
                
                OrdersGridView.Rows.Add(orders[i].id, tovarsList);
            }
        }
    }
}
