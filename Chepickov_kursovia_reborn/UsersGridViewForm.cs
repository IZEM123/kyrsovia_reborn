﻿using kursovaya_chepikov;
using kursovaya_chepikov.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Chepickov_kursovia_reborn
{
    public partial class UsersGridViewForm : TemplateForm
    {

        bool isRowEdit = false;
        int rowEditNumber = -1;
        public UsersGridViewForm()
        {
            InitializeComponent();
        }

        private void UsersGridViewForm_Load(object sender, EventArgs e)
        {
            loadAndDisplayUsersFromDB();
        }
        private void loadAndDisplayUsersFromDB()
        {
            DataBaseControl dbController = new DataBaseControl();
            List<Worker1> users = dbController.getUsers();

            displayUsers(users);
        }
        private void displayUsers(List<Worker1> users)
        {
            WorkersGridView.Rows.Clear();
            for (int i = 0; i < users.Count; i++)
            {
                WorkersGridView.Rows.Add(users[i].id, users[i].FullName, users[i].login, users[i].password,  Worker1.getRoleName(users[i].workerRole), "Edit", "Delete");
            }
        }

        private void UsersGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if(e.RowIndex < 0)
            {
                return;
            }
            if (e.ColumnIndex == 5)
            {
                if (!isRowEdit)
                {
                    startRowEdit(e.RowIndex);
                }
                else
                {
                    if (rowEditNumber == e.RowIndex)
                    {
                        saveRow();
                    }
                    else
                    {
                        saveRow();
                        startRowEdit(e.RowIndex);
                    }
                }
            }
            if (e.ColumnIndex == 6)
            {
                deleteRow(e.RowIndex);
            }
        }

        private void startRowEdit(int row)
        {
            isRowEdit = true;
            rowEditNumber = row;
            WorkersGridView.Rows[row].Cells[1].ReadOnly = false;
            WorkersGridView.Rows[row].Cells[2].ReadOnly = false;
            WorkersGridView.Rows[row].Cells[3].ReadOnly = false;
            WorkersGridView.Rows[row].Cells[4].ReadOnly = false;


            WorkersGridView.Rows[row].Cells[5].Value = "Save";
        }

        private void saveRow()
        {
            Worker1 user = new Worker1();
            user.login = WorkersGridView.Rows[rowEditNumber].Cells[1].Value.ToString();
            user.password = WorkersGridView.Rows[rowEditNumber].Cells[2].Value.ToString();
            user.FullName = WorkersGridView.Rows[rowEditNumber].Cells[3].Value.ToString();
            user.workerRole = Worker1.getRoleByName(WorkersGridView.Rows[rowEditNumber].Cells[3].Value.ToString());


            WorkersGridView.Rows[rowEditNumber].Cells[1].ReadOnly = true;
            WorkersGridView.Rows[rowEditNumber].Cells[2].ReadOnly = true;
            WorkersGridView.Rows[rowEditNumber].Cells[3].ReadOnly = true;
            WorkersGridView.Rows[rowEditNumber].Cells[4].ReadOnly = true;


            WorkersGridView.Rows[rowEditNumber].Cells[5].Value = "Edit";

            DataBaseControl dbController = new DataBaseControl();

            dbController.UpdateUser(rowEditNumber, user);

            isRowEdit = false;
            rowEditNumber = -1;
        }

        private void deleteRow(int rowId)
        {
            DataBaseControl dbController = new DataBaseControl();

            dbController.DeleteUser(rowId);

            loadAndDisplayUsersFromDB();
        }
    }
}

