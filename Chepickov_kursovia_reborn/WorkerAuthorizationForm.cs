﻿using kursovaya_chepikov;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Chepickov_kursovia_reborn
{
    public partial class WorkerAuthorizationForm : TemplateForm
    {
        public WorkerAuthorizationForm()
        {
            InitializeComponent();
        }

        private void WorkerAuthorizationForm_Load(object sender, EventArgs e)
        {

        }

        private void Submit_b_Click(object sender, EventArgs e)
        {
            DataBaseControl dbController = new DataBaseControl();

            string login = LoginInput.Text.Trim();
            string password = PasswordInput.Text.Trim();
            if (login != "" && password != "")
            {
                try
                {
                    dbController.AuthorizateUser(login, password);
                    WorkerMenuForm form = new WorkerMenuForm();
                    form.Owner = this.Owner;
                    form.Show();
                    this.Owner = null;
                    this.Close();
                }
                catch
                {
                    PasswordInput.Text = "";
                }
            }
        }
    }
}
