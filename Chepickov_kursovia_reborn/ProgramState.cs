﻿using kursovaya_chepikov.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kursovaya_chepikov
{
   
    class ProgramState
    {
        private static Worker1 AuthorizedUser;

        public void setAuthorizedUser(Worker1 user)
        {
            AuthorizedUser = user;
        }
        public void AuthorizedUserLogout()
        {
            AuthorizedUser = null;
        }
         
        public Worker1 GetAuthorizedUser()
        {
            return AuthorizedUser;
        }
    }
}
