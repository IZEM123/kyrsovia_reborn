﻿
namespace Chepickov_kursovia_reborn
{
    partial class WorkerAddForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.FullNameInput = new System.Windows.Forms.TextBox();
            this.WorkerRoleComboBox = new System.Windows.Forms.ComboBox();
            this.LoginInput = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.PasswordInput = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Submit_b = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(95, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "ФИО";
            // 
            // FullNameInput
            // 
            this.FullNameInput.Location = new System.Drawing.Point(89, 81);
            this.FullNameInput.Name = "FullNameInput";
            this.FullNameInput.Size = new System.Drawing.Size(100, 20);
            this.FullNameInput.TabIndex = 1;
            // 
            // WorkerRoleComboBox
            // 
            this.WorkerRoleComboBox.FormattingEnabled = true;
            this.WorkerRoleComboBox.Items.AddRange(new object[] {
            "client",
            "admin",
            "worker"});
            this.WorkerRoleComboBox.Location = new System.Drawing.Point(89, 244);
            this.WorkerRoleComboBox.Name = "WorkerRoleComboBox";
            this.WorkerRoleComboBox.Size = new System.Drawing.Size(121, 21);
            this.WorkerRoleComboBox.TabIndex = 2;
            // 
            // LoginInput
            // 
            this.LoginInput.Location = new System.Drawing.Point(89, 133);
            this.LoginInput.Name = "LoginInput";
            this.LoginInput.Size = new System.Drawing.Size(100, 20);
            this.LoginInput.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(95, 117);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Логин";
            // 
            // PasswordInput
            // 
            this.PasswordInput.Location = new System.Drawing.Point(89, 181);
            this.PasswordInput.Name = "PasswordInput";
            this.PasswordInput.PasswordChar = '*';
            this.PasswordInput.Size = new System.Drawing.Size(100, 20);
            this.PasswordInput.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(95, 165);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Пароль";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(94, 221);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 20);
            this.label4.TabIndex = 7;
            this.label4.Text = "Роль";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // Submit_b
            // 
            this.Submit_b.Location = new System.Drawing.Point(89, 298);
            this.Submit_b.Name = "Submit_b";
            this.Submit_b.Size = new System.Drawing.Size(121, 23);
            this.Submit_b.TabIndex = 8;
            this.Submit_b.Text = "Зарегистрировать";
            this.Submit_b.UseVisualStyleBackColor = true;
            this.Submit_b.Click += new System.EventHandler(this.Submit_b_Click);
            // 
            // WorkerAddForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Submit_b);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.PasswordInput);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.LoginInput);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.WorkerRoleComboBox);
            this.Controls.Add(this.FullNameInput);
            this.Controls.Add(this.label1);
            this.Name = "WorkerAddForm";
            this.Text = "WorkerAddForm";
            this.Load += new System.EventHandler(this.WorkerAddForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox FullNameInput;
        private System.Windows.Forms.ComboBox WorkerRoleComboBox;
        private System.Windows.Forms.TextBox LoginInput;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox PasswordInput;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button Submit_b;
    }
}