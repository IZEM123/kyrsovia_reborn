﻿using Chepickov_kursovia_reborn.DataBas;
using kursovaya_chepikov;
using kursovaya_chepikov.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Chepickov_kursovia_reborn
{
    
    public partial class OrderForm : TemplateForm
    {

        List<TovarInCart> cart = new List<TovarInCart>();

        public OrderForm()
        {
            InitializeComponent();
        }

        private void OrderForm_Load(object sender, EventArgs e)
        {
            DataBaseControl dbController = new DataBaseControl();
            List<Tovar> tovars = dbController.GetTovars();
            for(int i = 0; i<tovars.Count; i++)
            {
                TovarsGridView.Rows.Add(tovars[i].id,tovars[i].title,tovars[i].price,"1","Добавить в корзину") ;
            }
        }

        private void TovarListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
      

            
        }

        private void TovarsGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataBaseControl dbController = new DataBaseControl();
            if (e.ColumnIndex == 4)
            {
                int tovarId = Convert.ToInt32(TovarsGridView.Rows[e.RowIndex].Cells[0].Value);
                int count = Convert.ToInt32(TovarsGridView.Rows[e.RowIndex].Cells[3].Value);
                if (count < 1)
                {
                    MessageBox.Show("Некорректное количество товара");
                    return;
                }
                Tovar tovar = dbController.GetTovarById(tovarId);

                for (int i = 0; i < cart.Count; i++)
                {
                    if (cart[i].tovar.id == tovarId)
                    {
                        if(cart[i].count+count > tovar.count)
                        {
                            MessageBox.Show($"Такого количества ({count}) товара нет на складе ");
                            return;
                        }
                        cart[i].count+= count;
                        DisplayCart();
                        return;
                    }
                }

                if (count > tovar.count)
                {
                    MessageBox.Show($"Такого количества ({count}) товара нет на складе ");
                    return;
                }
                TovarInCart newTovar = new TovarInCart();
                newTovar.tovar = tovar; 
                newTovar.count = count;
                cart.Add(newTovar);
                DisplayCart();

            }
        }

        private void DisplayCart()
        {
            CartGridView.Rows.Clear();
            double totalSum = 0;
            
            for(int i = 0; i<cart.Count; i++)
            {
                CartGridView.Rows.Add(cart[i].tovar.title, cart[i].count, cart[i].tovar.price * cart[i].count);
                totalSum += cart[i].tovar.price * cart[i].count;
            }
            try
            {
                SummOrderNumeric.Value = (decimal)totalSum;
            }
            catch (Exception) { }


        }

        private void SummOrderNumeric_ValueChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            DataBaseControl dbController = new DataBaseControl();

            Order order = new Order();
            order.tovars = cart;
            dbController.AddOrder(order);
            this.Close();

            CheckForm form = new CheckForm(order);
            form.Show();

        }

    }

    public class TovarInCart
    {
        public Tovar tovar;
        public int count;
    }

}
