﻿
namespace Chepickov_kursovia_reborn
{
    partial class WorkerMenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.AddTovar_b = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.GridView_b = new System.Windows.Forms.Button();
            this.AddWorker_b = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(548, 266);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(127, 45);
            this.button1.TabIndex = 5;
            this.button1.Text = "История заказов";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // AddTovar_b
            // 
            this.AddTovar_b.Location = new System.Drawing.Point(103, 130);
            this.AddTovar_b.Name = "AddTovar_b";
            this.AddTovar_b.Size = new System.Drawing.Size(127, 45);
            this.AddTovar_b.TabIndex = 4;
            this.AddTovar_b.Text = "Добавить товар";
            this.AddTovar_b.UseVisualStyleBackColor = true;
            this.AddTovar_b.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(103, 69);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(127, 45);
            this.button4.TabIndex = 3;
            this.button4.Text = "Оформить заказ";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(103, 195);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(127, 45);
            this.button3.TabIndex = 2;
            this.button3.Text = "Список товаров";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // GridView_b
            // 
            this.GridView_b.Location = new System.Drawing.Point(103, 266);
            this.GridView_b.Name = "GridView_b";
            this.GridView_b.Size = new System.Drawing.Size(127, 45);
            this.GridView_b.TabIndex = 1;
            this.GridView_b.Text = "Список работников";
            this.GridView_b.UseVisualStyleBackColor = true;
            this.GridView_b.Click += new System.EventHandler(this.button2_Click);
            // 
            // AddWorker_b
            // 
            this.AddWorker_b.Location = new System.Drawing.Point(103, 329);
            this.AddWorker_b.Name = "AddWorker_b";
            this.AddWorker_b.Size = new System.Drawing.Size(127, 45);
            this.AddWorker_b.TabIndex = 0;
            this.AddWorker_b.Text = "Добавить работника";
            this.AddWorker_b.UseVisualStyleBackColor = true;
            this.AddWorker_b.Click += new System.EventHandler(this.button1_Click);
            // 
            // WorkerMenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.AddTovar_b);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.GridView_b);
            this.Controls.Add(this.AddWorker_b);
            this.Name = "WorkerMenuForm";
            this.Text = "Меню работника";
            this.Load += new System.EventHandler(this.WorkerMenuForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button AddWorker_b;
        private System.Windows.Forms.Button GridView_b;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button AddTovar_b;
        private System.Windows.Forms.Button button1;
    }
}