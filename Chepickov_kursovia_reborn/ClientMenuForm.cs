﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Chepickov_kursovia_reborn
{
    public partial class ClientMenuForm : TemplateForm
    {
        public ClientMenuForm()
        {
            InitializeComponent();
        }

        private void ClientMenuForm_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            TovarGridVeiwForm form = new TovarGridVeiwForm(true);
            this.Hide();
            form.Show();
            form.Owner = this;
        }
    }
}
