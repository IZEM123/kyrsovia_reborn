﻿
namespace Chepickov_kursovia_reborn
{
    partial class TovarAddForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.TitleInput = new System.Windows.Forms.TextBox();
            this.ProviderInput = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.PriceInput = new System.Windows.Forms.NumericUpDown();
            this.CountInput = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.PriceInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CountInput)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(75, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Название товара";
            // 
            // TitleInput
            // 
            this.TitleInput.Location = new System.Drawing.Point(70, 81);
            this.TitleInput.Name = "TitleInput";
            this.TitleInput.Size = new System.Drawing.Size(112, 20);
            this.TitleInput.TabIndex = 1;
            // 
            // ProviderInput
            // 
            this.ProviderInput.Location = new System.Drawing.Point(70, 141);
            this.ProviderInput.Name = "ProviderInput";
            this.ProviderInput.Size = new System.Drawing.Size(112, 20);
            this.ProviderInput.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(75, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Поставщик";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(75, 182);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Цена";
            // 
            // PriceInput
            // 
            this.PriceInput.Location = new System.Drawing.Point(70, 209);
            this.PriceInput.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.PriceInput.Name = "PriceInput";
            this.PriceInput.Size = new System.Drawing.Size(120, 20);
            this.PriceInput.TabIndex = 5;
            // 
            // CountInput
            // 
            this.CountInput.Location = new System.Drawing.Point(70, 267);
            this.CountInput.Maximum = new decimal(new int[] {
            -727379969,
            232,
            0,
            0});
            this.CountInput.Name = "CountInput";
            this.CountInput.Size = new System.Drawing.Size(120, 20);
            this.CountInput.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(75, 240);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Количество";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(70, 320);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(137, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Добавить товар";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // TovarAddForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.CountInput);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.PriceInput);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ProviderInput);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TitleInput);
            this.Controls.Add(this.label1);
            this.Name = "TovarAddForm";
            this.Text = "TovarAddForm";
            this.Load += new System.EventHandler(this.TovarAddForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PriceInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CountInput)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TitleInput;
        private System.Windows.Forms.TextBox ProviderInput;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown PriceInput;
        private System.Windows.Forms.NumericUpDown CountInput;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
    }
}